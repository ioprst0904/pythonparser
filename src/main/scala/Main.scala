import fastparse._
import fastparse.NoWhitespace._

import pyparse._
import Interpreter.run

object Main extends App {
  def read_file(path: String): String = {
    val src = io.Source.fromFile(path)
    val code = src.getLines().mkString("\n")
    src.close()
    code
  }

  def parse_code(code: String): Parsed[Seq[Ast.stmt]] = {
    def parseIt[_: P]: P[Seq[Ast.stmt]] = Statements.file_input(P.current) ~ End
    parse(code, parseIt(_))
  }

  parse_code(read_file("src/main/scala/test.py")) match {
    case f: Parsed.Failure => throw new Exception(f.trace().longTerminalsMsg)
    case s: Parsed.Success[Seq[parsed]] =>
      val result: Seq[Ast.stmt] = s.value
      run(result)
  }
}
