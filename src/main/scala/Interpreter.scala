import scala.collection.mutable.ListBuffer

import pyparse.Ast._

class Stack[T] {
  var items: ListBuffer[T] = new ListBuffer[T]()

  def push(value: T): Unit = items.insert(0, value)

  def pop(): Any = {
    if (items.nonEmpty) {
      val value = items.head
      items.remove(0)
      value
    } else {
      None
    }
  }

  def head: Any = {
    if (length > 0) {
      items.head
    } else {
      None
    }
  }

  def length: Int = items.length

  def next: Any = {
    if (length > 1) {
      items(1)
    } else {
      None
    }
  }

  def clear(): Unit = items.clear()
}

//class Storage[T] {
//  var items = Map[String, T]()
//
//  def append(name: String, value: T): Unit = items = items + (name -> value)
//
//  def remove(name: String): Unit = items = items - name
//
//  def get(name: String): Any = {
//    if (exists(name)) {
//      items(name)
//    } else {
//      None
//    }
//  }
//
//  def exists(name: String): Boolean = items.keySet.contains(name)
//}

class VariableStorage {
  var items: Map[String, Any] = Map[String, Any]()

  def append(name: String, value: Any): Unit = items = items + (name -> value)

  def remove(name: String): Unit = items = items - name

  def get(name: String): Any = {
    if (exists(name)) {
      items(name)
    } else {
      None
    }
  }

  def exists(name: String): Boolean = items.keySet.contains(name)
}

class Func(val name: String, val args: Seq[expr], val body: Seq[stmt])

class FunctionStorage {
  var items: Map[String, Func] = Map[String, Func]()

  def append(name: String, args: Seq[expr], body: Seq[stmt]): Unit = items = items + (name -> new Func(name, args, body))

  def remove(name: String): Unit = items = items - name

  def get(name: String): Object = {
    if (exists(name)) {
      items(name)
    } else {
      None
    }
  }

  def exists(name: String): Boolean = items.keySet.contains(name)
}

object Interpreter {
  // Хранилища глобальных объектов
  val baseVars = new VariableStorage
  baseVars.append("True", 1)
  baseVars.append("False", 0)
  baseVars.append("None", None)
  var globalVars = new VariableStorage
  var globalFunctions: Map[String, Func] = Map[String, Func]()

  def getVariable(name: String, vars: VariableStorage): Any = {
    /*
    Получить значение переменной name. Сначала проверить наличие в объекте vars - локальная переменная,
    если там переменная отсутствует, то проверить наличие глобальной перменной. Если в глобальных переменных
    переменная name отсутствует, то вернуть None.
    */
    if (baseVars.exists(name)) {
      baseVars.get(name)
    } else if (vars.exists(name)) {
      vars.get(name)
    } else if (globalVars.exists(name)) {
      globalVars.get(name)
    } else {
      None
    }
  }

  def evalExpr(expression: expr, vars: VariableStorage): Any = {
    /*
    Вычисление выражения

    @param expression выражение
    @param vars хранилище переменных
    */
    expression match {
      case expr.Num(n) => n.toString.toInt
      case expr.Str(s) => s
      case expr.BinOp(left, op, right) => BinOp(left, op, right, vars)
      case expr.Name(id, ctx) => getVariable(id.name, vars)
      case expr.Call(func, args, keywords, starargs, kwargs) => Call(func, args, keywords, starargs, kwargs, vars)
      case expr.Compare(left, ops, comparators) => Compare(left, ops, comparators, vars)
    }
  }

  def Assign(targets: Seq[expr], value: expr, vars: VariableStorage): Unit = {
    targets.head match {
      case expr.Name(id, ctx) => vars.append(id.name, evalExpr(value, vars))
      case expr.Tuple(elts: List[expr.Name], ctx) =>
        value match {
          case expr.Tuple(elts_value: List[expr], ctx) =>
            for (i <- elts.indices) {
              vars.append(elts(i).id.name, evalExpr(elts_value(i), vars))
            }
          case _ =>
            for (name <- elts) {
              vars.append(name.id.name, evalExpr(value, vars))
            }
        }
    }
  }

  def BinOpInt(left: Int, op: operator, right: Int): Int = {
    op match {
      case operator.Add => left + right
      case operator.Sub => left - right
      case operator.Mult => left * right
      case operator.Div => left / right
      case operator.Mod => left % right
      case operator.Pow => math.pow(left, right).toInt
    }
  }

  def BinOpString(left: String, op: operator, right: String): String = {
    op match {
      case operator.Add => left + right
    }
  }

  def BinOpStringInt(left: String, op: operator, right: Int): String = {
    op match {
      case operator.Mult => left * right
    }
  }

  def BinOp(left: expr, op: operator, right: expr, vars: VariableStorage): Any = {
    val l = evalExpr(left, vars)
    val r = evalExpr(right, vars)

    l match {
      case n_left: Int =>
        r match {
          case n_right: Int => BinOpInt(n_left, op, n_right)
          case s_right: String => BinOpStringInt(s_right, op, n_left)
        }
      case s_left: String =>
        r match {
          case n_right: Int => BinOpStringInt(s_left, op, n_right)
          case s_right: String => BinOpString(s_left, op, s_right)
        }
    }
  }

  def Print(dest: Option[expr], values: Seq[expr], vars: VariableStorage, end: String = "\n"): Unit = {
    values.head match {
      case expr.Tuple(elts, ctx) =>
        val len = elts.length
        for (i <- 0.to(len-2)) {
          Print(None, List(elts(i)), vars, end=" ")
        }
        Print(None, List(elts(len-1)), vars)
      case v =>
        v match {
          case expr.Name(id, ctx) =>
            if (Seq("True", "False").contains(id.name)) {
              print(id.name + end)
            } else {
              print(evalExpr(v, vars).toString + end)
            }
          case _ => print(evalExpr(v, vars).toString + end)
        }
    }
  }

  def FunctionDef(name: identifier, args: arguments, body: Seq[stmt]): Unit = {
    globalFunctions = globalFunctions + (name.name -> new Func(name.name, args.args, body))
  }

  def Call(func: expr, args: Seq[expr], keywords: Seq[keyword], starargs: Option[expr],
           kwargs: Option[expr], vars: VariableStorage): Any = {
    val func_name = func match {
      case expr.Name(id, ctx) => id.name
    }
    val function = globalFunctions(func_name)
    val function_args = function.args
    val function_body = function.body

    val localVars = new VariableStorage
//    val localFuncs = Map[String, Func]()

    for (i <- function_args.indices) {
      function_args(i) match {
        case expr.Name(id, ctx) => localVars.append(id.name, evalExpr(args(i), vars))
        case err => println("Что это:" + err + "?")
      }
    }

    interpret(function_body, localVars)
  }

  def Return(value: Option[expr], vars: VariableStorage): Any = {
    val result = value.head
    result match {
      case e => evalExpr(e, vars)
    }
  }

  def Compare(left: expr, ops: Seq[cmpop], comparators: Seq[expr], vars: VariableStorage): Int = {
    var l = evalExpr(left, vars)
    var result = true
    for (i <- ops.indices) {
      val op = ops(i)
      val r = evalExpr(comparators(i), vars)
      result = op match {
        case cmpop.Eq => result && l == r
        case cmpop.NotEq => result && l != r
        case cmpop.Lt => result && l.toString.toInt < r.toString.toInt
        case cmpop.LtE => result && l.toString.toInt <= r.toString.toInt
        case cmpop.Gt => result && l.toString.toInt > r.toString.toInt
        case cmpop.GtE => result && l.toString.toInt >= r.toString.toInt
      }
      l = r
    }
    if (result) 1 else 0
  }

  def If(test: expr, body: Seq[stmt], orelse: Seq[stmt], vars: VariableStorage): Unit = {
    val testResult: Int = test match {
      case expr.Compare(left, ops, comparators) => Compare(left, ops, comparators, vars)
      case exp =>
        val value = evalExpr(exp, vars)
        value match {
          case n: Int => if (n != 0) 1 else 0
          case s: String => if (s != "") 1 else 0
        }
    }

    if (testResult == 1) {
      interpret(body, vars)
    } else {
      interpret(orelse, vars)
    }
  }

  var callStack = new Stack[VariableStorage]
  var flgReturn = false
  var resultReturn: Any = None

  def interpret(body: Seq[stmt], vars: VariableStorage): Any = {
    /*
    Интерпретировать блок
    */
    callStack.push(vars)

    var result: Any = None

    for (i <- body.indices) {
      val action = body(i)
      if (!flgReturn) {
        action match {
          case stmt.Assign(targets, value) => Assign(targets, value, vars)
          case stmt.Print(dest, values, nl) => Print(dest, values, vars)
          case stmt.FunctionDef(name, args, body, decorator_list) => FunctionDef(name, args, body)
          case stmt.Expr(value) =>
            value match {
              case expr.Call(func, args, keywords, starargs, kwargs) => Call(func, args, keywords, starargs, kwargs, vars)
            }
          case stmt.If(test, body, orelse) => If(test, body, orelse, vars)
          case stmt.Return(value) =>
            result = Return(value, vars)
            resultReturn = result
            flgReturn = true
          case _ => println("interpreter: что это: " + action + "?")
        }
      }

      else {
        if (callStack.head == vars) {
          result = resultReturn
          if (callStack.length > 1) {
            if (callStack.next != vars) {
              flgReturn = false
            }
          }
        }
      }

      // Т.к. flgReturn проверяется в начале итерации, то, если полсе return нет
      // больше кода => это последняя итерация цикла по body, флаг flgReturn
      // не будет сброшен, т.к. не будет выполнена проверка условия "if (!flgReturn)",
      // ложный результат которого приводит к потенциальному сбросу флага flgReturn.
      // Поэтому добавлена обработка последней итерации по body.
      if (i == body.length - 1) {
        result = resultReturn
        if (flgReturn) {
          if (callStack.length > 1) {
            if (callStack.next != vars) {
              flgReturn = false
            }
          }
        }
      }
    }

    callStack.pop()
    result
  }

  def run(body: Seq[stmt]): Unit = {
    interpret(body, globalVars)
  }
}
